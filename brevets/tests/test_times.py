'''
Alex Marozick

Nose tests for acp_times.py
'''
from acp_times import open_time
from acp_times import close_time
import nose
import arrow


def test_open_time():
    #open_time(control_dist_km, brevet_dist_km, brevet_start_time)
    start_time = arrow.get(2020, 5, 14)

    assert open_time(0, 300, start_time) == start_time.shift(hours = 0, minutes = 0).isoformat()
    assert open_time(100, 300, start_time) == start_time.shift(hours = 2, minutes = 56).isoformat()
    assert open_time(200, 300, start_time) == start_time.shift(hours = 5, minutes = 53).isoformat()
    assert open_time(300, 300, start_time) == start_time.shift(hours = 9, minutes = 0).isoformat()
    assert open_time(360, 300, start_time) == start_time.shift(hours = 9, minutes = 0).isoformat()

    assert open_time(0, 1000, start_time) == start_time.shift(hours = 0, minutes = 0).isoformat()
    assert open_time(200, 1000, start_time) == start_time.shift(hours = 5, minutes = 53).isoformat()
    assert open_time(400, 1000, start_time) == start_time.shift(hours = 12, minutes = 8).isoformat()
    assert open_time(600, 1000, start_time) == start_time.shift(hours = 18, minutes = 48).isoformat()
    assert open_time(1000, 1000, start_time) == start_time.shift(hours = 33, minutes = 5).isoformat()
    assert open_time(1200, 1000, start_time) == start_time.shift(hours = 33, minutes = 5).isoformat()



def test_close_time():
    #close_time(control_dist_km, brevet_dist_km, brevet_start_time)
    start_time = arrow.get(2020, 5, 14)

    assert close_time(0, 300, start_time) == start_time.shift(hours = 1, minutes = 0).isoformat()
    assert close_time(100, 300, start_time) == start_time.shift(hours = 6, minutes = 40).isoformat()
    assert close_time(200, 300, start_time) == start_time.shift(hours = 13, minutes = 20).isoformat()
    assert close_time(300, 300, start_time) == start_time.shift(hours = 20, minutes = 0).isoformat()
    assert close_time(360, 300, start_time) == start_time.shift(hours = 20, minutes = 0).isoformat()

    assert close_time(0, 1000, start_time) == start_time.shift(hours = 1, minutes = 0).isoformat()
    assert close_time(200, 1000, start_time) == start_time.shift(hours = 13, minutes = 20).isoformat()
    assert close_time(400, 1000, start_time) == start_time.shift(hours = 26, minutes = 40).isoformat()
    assert close_time(600, 1000, start_time) == start_time.shift(hours = 40, minutes = 0).isoformat()
    assert close_time(1000, 1000, start_time) == start_time.shift(hours = 75, minutes = 0).isoformat()
    assert close_time(1200, 1000, start_time) == start_time.shift(hours = 75, minutes = 0).isoformat()


test_open_time()
test_close_time()
